﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundMenuController : MonoBehaviour
{
    public Slider[] sliders;
    public AudioSource AudioSourceSE;
    public AudioSource AudioSourceBGM;
    // Start is called before the first frame update
    void Start()
    {
        sliders[0].value = PlayerPrefs.GetFloat("MusicaVolume", 1);
        sliders[1].value = PlayerPrefs.GetFloat("EfeitoVolume", 1);
        sliders[2].value = PlayerPrefs.GetFloat("GeralVolume", 1);
    }

    // Update is called once per frame
    void Update()
    {
        getVolumes();
    }

    public void setVolumes()
    {
        PlayerPrefs.SetFloat("MusicaVolume", sliders[0].value);
        PlayerPrefs.SetFloat("EfeitoVolume", sliders[1].value);
        PlayerPrefs.SetFloat("GeralVolume", sliders[2].value);

    }

    private void getVolumes()
    {
        AudioSourceBGM.volume = (PlayerPrefs.GetFloat("MusicaVolume", 1) * PlayerPrefs.GetFloat("GeralVolume", 1));
        AudioSourceSE.volume = (PlayerPrefs.GetFloat("EfeitoVolume", 1) * PlayerPrefs.GetFloat("GeralVolume", 1));
    }
}
