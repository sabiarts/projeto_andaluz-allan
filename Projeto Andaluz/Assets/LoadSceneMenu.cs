﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadSceneMenu : MonoBehaviour
{
    bool canLoad;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    public void AtividadesEmBaia(int minigame)
    {
        StartCoroutine(waitToLoadBaia(minigame));
    }
    public void Equoterapia(int minigame)
    {
        StartCoroutine(waitToLoadEquoterapia(minigame));
        
    }
    public void PreEquitacao(int minigame)
    {
        StartCoroutine(waitToLoadPre(minigame));
    }

    IEnumerator waitToLoadBaia(int minigame)
    {
        yield return new WaitForSeconds(2f);
        switch (minigame)
        {
            case 1:
                PlayerPrefs.SetString("MinigameAt", "MinigameEleven");
                break;
            case 2:
                PlayerPrefs.SetString("MinigameAt", "MinigameTwelve");
                break;
            case 3:
                PlayerPrefs.SetString("MinigameAt", "MinigameThirteen");
                break;
            case 4:
                PlayerPrefs.SetString("MinigameAt", "MinigameFourteen");
                break;
            case 5:
                PlayerPrefs.SetString("MinigameAt", "MinigameFifteen");
                break;
        }
        SceneManager.LoadScene("MinigameEleven");
    }
    IEnumerator waitToLoadEquoterapia(int minigame)
    {
        yield return new WaitForSeconds(2f);
        switch (minigame)
        {
            case 1:
                PlayerPrefs.SetString("MinigameAt", "MinigameThree");
                SceneManager.LoadScene("MinigameThree");
                break;
            case 2:
                PlayerPrefs.SetString("MinigameAt", "MinigameOne");
                SceneManager.LoadScene("MinigameOne");
                break;
            case 3:
                PlayerPrefs.SetString("MinigameAt", "MinigameTwo");
                SceneManager.LoadScene("MinigameTwo");
                break;
            case 4:
                PlayerPrefs.SetString("MinigameAt", "MinigameTen");
                SceneManager.LoadScene("MinigameTen");
                break;
            case 5:
                PlayerPrefs.SetString("MinigameAt", "MinigameFour");
                SceneManager.LoadScene("MinigameFour");
                break;
        }

    }
    IEnumerator waitToLoadPre(int minigame)
    {
        yield return new WaitForSeconds(2f);
        switch (minigame)
        {
            case 1:
                PlayerPrefs.SetString("MinigameAt", "MinigameFive");
                SceneManager.LoadScene("MinigameEight");
                break;
            case 2:
                PlayerPrefs.SetString("MinigameAt", "MinigameSeven");
                SceneManager.LoadScene("MinigameEight");
                break;
            case 3:
                PlayerPrefs.SetString("MinigameAt", "MinigameSix");
                SceneManager.LoadScene("MinigameEight");
                break;
            case 4:
                PlayerPrefs.SetString("MinigameAt", "MinigameNine");
                SceneManager.LoadScene("MinigameNine");
                break;
            case 5:
                PlayerPrefs.SetString("MinigameAt", "MinigameNine2");
                SceneManager.LoadScene("MinigameNine2");
                break;
        }
    }
}
