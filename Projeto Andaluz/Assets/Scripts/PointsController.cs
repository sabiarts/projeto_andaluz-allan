﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PointsController : MonoBehaviour
{
    public Text pointsInLevelText;
    public Text totalPointsText;

    public float pointsInLevel;
    public float totalPoints;

    public void Start()
    {
        totalPoints = PlayerPrefs.GetFloat("TotalPoints");
        totalPointsText.text = totalPoints.ToString();
        pointsInLevelText.text = pointsInLevel.ToString();
    }
    private void Update()
    {

    }
    public void AddPoints(int points)
    {
        pointsInLevel += points;
        totalPoints += points;
        totalPointsText.text = totalPoints.ToString();
        pointsInLevelText.text = pointsInLevel.ToString();
    }
    public void LosePoints()
    {
        if(pointsInLevel > 0)
        {
            //teste
            pointsInLevel -= 25;
            totalPoints -= 25;
            totalPointsText.text = totalPoints.ToString();
            pointsInLevelText.text = pointsInLevel.ToString();
        }
        else if (pointsInLevel <=0)
        {
            pointsInLevel = 0;
            pointsInLevelText.text = pointsInLevel.ToString();
        }
        
    }

    public void TotalPoints()
    {
        PlayerPrefs.SetFloat("TotalPoints", totalPoints);
    }

    public void ResetPoints()
    {
        pointsInLevel = 0;
    }

    public void HighScorePoints()
    {

    }

    public void ResetAllThePoints()
    {
        PlayerPrefs.SetFloat("TotalPoints", 0);
        pointsInLevel = 0;
        pointsInLevelText.text = pointsInLevel.ToString();
        totalPointsText.text = totalPoints.ToString();
    }
}
