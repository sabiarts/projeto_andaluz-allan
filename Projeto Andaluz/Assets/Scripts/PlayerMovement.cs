﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    public float speed = 10;

    private Rigidbody rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        float xAxys = Input.GetAxis("Horizontal");
        float zAxys = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(xAxys, 0, zAxys) * speed * Time.deltaTime;

        rb.MovePosition(transform.position + movement);
    }
}
