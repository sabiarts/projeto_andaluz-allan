﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Pause : MonoBehaviour
{
    public GameObject carregando;
    public SoundEffectController soundEffectController;
    public void PauseGame()
    {
        StartCoroutine(WaitingToFreezeGame());
        soundEffectController.PlayAudio("Botão_Ir", true);
    }
    public void ContinueGame()
    {
        Time.timeScale = 1;
        soundEffectController.PlayAudio("Botão_Voltar", true);
    }
    public void RestartGame()
    {
        Time.timeScale = 1;
        carregando.SetActive(true);
        StartCoroutine(CarregandoAnim());
        soundEffectController.PlayAudio("Botão_Ir", true);
    }

    IEnumerator WaitingToFreezeGame()
    {
        yield return new WaitForSeconds(0.5f);
        Time.timeScale = 0;
    }

    IEnumerator CarregandoAnim()
    {
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
