﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AxysCorrection : MonoBehaviour
{
    float pointToMove;
    float direction;
    bool automaticRotate;
    GameController gameController;
    public Slider axysController;
    // Start is called before the first frame update
    void Start()
    {
        gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
    }

    // Update is called once per frame
    void Update()
    {

        pointToMove = axysController.value;
        if (Input.GetMouseButtonDown(0) || Input.GetMouseButton(0))
        {
            if (pointToMove > 0.55f )
            {
                direction = -1;
            }
            else if (pointToMove < 0.45f )
            {
                direction = 1;
            }

            RotateObject();
            
        }
        if (!Input.GetMouseButton(0) && automaticRotate)
        {
            if(this.gameObject.transform.rotation.z > 0)
            {
                direction = 0.5f;
                RotateObject();
            }
            else
            {
                direction = -0.5f;
                RotateObject();
            }

        }
        if (this.gameObject.transform.rotation.z < -0.45 || this.gameObject.transform.rotation.z > 0.45)
        {
            automaticRotate = false;
            gameController.setDicasLose();
        }
        else
        {
            automaticRotate = true;
        }

    }

    void RotateObject()
    {
        this.gameObject.transform.rotation = new Quaternion(this.gameObject.transform.rotation.x, this.gameObject.transform.rotation.y, 
            this.gameObject.transform.rotation.z + (0.003f * direction), this.gameObject.transform.rotation.w);
    }
}
