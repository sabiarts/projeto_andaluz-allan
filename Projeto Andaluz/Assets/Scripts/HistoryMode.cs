﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HistoryMode : MonoBehaviour
{
    public GameObject IntroCutscene;
    public bool endedCutScene;
    public SoundEffectController soundEffectController;
    public AudioSource AudioSourceSE;
    public AudioSource AudioSourceBGM;
    // Start is called before the first frame update
    void Start()
    {
        if(PlayerPrefs.GetInt("InicioJaVisto") == 1)
        {
            IntroCutscene.SetActive(false);
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (endedCutScene)
        {
            StartCoroutine(WaitToStart());
        }
        AudioSourceBGM.volume = (PlayerPrefs.GetFloat("MusicaVolume", 1) * PlayerPrefs.GetFloat("GeralVolume", 1));
        AudioSourceSE.volume = (PlayerPrefs.GetFloat("EfeitoVolume", 1) * PlayerPrefs.GetFloat("GeralVolume", 1));

    }
    public void InicioJaVisto()
    {
        PlayerPrefs.SetInt("InicioJaVisto", 1);
    }

    public void EnterHistoryMode()
    {
        endedCutScene = true;
        soundEffectController.PlayAudio("Botão_Ir", true);
        PlayerPrefs.SetString("MinigameAt", "MinigameEleven");
    }
    public void ContinueHistoryMode()
    {
        soundEffectController.PlayAudio("Botão_Ir", true);
        if (PlayerPrefs.GetString("MinigameAt") == "MinigameFive" || PlayerPrefs.GetString("MinigameAt") == "MinigameSix" || PlayerPrefs.GetString("MinigameAt") == "MinigameSeven")
        {
            SceneManager.LoadScene("MinigameEight");
        }
        else if(PlayerPrefs.GetString("MinigameAt") == "BaiaTransitions" ||
            PlayerPrefs.GetString("MinigameAt") == "MinigameTwelve" ||
            PlayerPrefs.GetString("MinigameAt") == "MinigameThirteen" || 
            PlayerPrefs.GetString("MinigameAt") == "MinigameFourteen" || 
            PlayerPrefs.GetString("MinigameAt") == "MinigameFifteen")
        {
            SceneManager.LoadScene("MinigameEleven");
        }
        else
        {
            SceneManager.LoadScene(PlayerPrefs.GetString("MinigameAt"));
        }
        
    }
    public void EndedCutScene()
    {
        endedCutScene = true;
    }

    IEnumerator WaitToStart()
    {
        yield return new WaitForSeconds(2f);
        SceneManager.LoadScene("MinigameEleven");
    }
}
