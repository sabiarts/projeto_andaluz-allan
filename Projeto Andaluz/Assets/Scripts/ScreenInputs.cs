﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenInputs : MonoBehaviour
{
    //Define aonde podemos clicar no cenário 3D.
    public LayerMask whatCanBeClickedOn;
    public LayerMask whatObjectCanBeClickedOn;
    //Define a distancia que botemos clicar com base na camera.
    public float maximumRayLenght;
    public RaycastHit hit;
    public RaycastHit objectHit;
    public Vector3 clickPosition;
    Vector3 startSwipePosition;
    Vector3 endSwipePosition;
    float minRangeSwipeDetection = 0.5f;
    public bool fingerOnScreen;
    public bool movingFinger;
    void Update()
    {
        Touch touch = Input.GetTouch(0);
        //Click and Hold Detection
        if (touch.phase == TouchPhase.Began)
        {
            fingerOnScreen = true;
            clickedRecognition(touch);
            //Debug.Log("Ponto na tela: "+ clickPosition);
        }
        else if(touch.phase == TouchPhase.Stationary)
        {
            movingFinger = false;
        }
        else if(touch.phase == TouchPhase.Moved)
        {
            movingFinger = true;
            clickedRecognition(touch);
        }
        else if(touch.phase == TouchPhase.Ended)
        {
            fingerOnScreen = false;
            movingFinger = false;
        }
    }

    public void clickedRecognition(Touch touch)
    {
        Ray mouseClickRay = Camera.main.ScreenPointToRay(touch.position);
        if (Physics.Raycast(mouseClickRay, out hit, maximumRayLenght, whatCanBeClickedOn))
        {
            //Debug.Log("Ponto no cenario: " + hit.point);
        }

        if (Physics.Raycast(mouseClickRay, out objectHit, maximumRayLenght, whatObjectCanBeClickedOn))
        {
            //Debug.Log("Ponto no cenario: " + objectHit.point);
            Debug.Log(objectHit.collider.tag);
        }

        clickPosition = Camera.main.ScreenToViewportPoint(touch.position);
    }
}
