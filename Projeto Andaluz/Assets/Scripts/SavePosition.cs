﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SavePosition : MonoBehaviour
{
    public Transform positionToSpawnHorseInStable;
    public GameObject horse;
    public GameObject horseInStable;
    public GameController gameController;
    public GameObject portaoBaia;
    // Start is called before the first frame update
    void Start()
    {
        switch (PlayerPrefs.GetString("MinigameAt"))
        {
            case "MinigameFive":
                break;
            case "MinigameSix":
                break;
            case "MinigameSeven":
                break;
            case "MinigameEight":
                break;

            case "MinigameTwelve":
                gameController.buttonPressedBaia("Escovar o Cavalo");
                horse.SetActive(false);
                horseInStable.SetActive(true);
                horseInStable.transform.position = positionToSpawnHorseInStable.position;
                horseInStable.transform.rotation = new Quaternion(0, 90, 0, 90);
                horseInStable.GetComponent<MoveThrowPaths>().enabled = false;
                gameController.happinnesImages.SetActive(true);
                gameController.happinnesName.SetActive(true);
                gameController.horseHappinnes.gameObject.SetActive(true);
                portaoBaia.SetActive(false);
                break;
            case "MinigameThirteen":
                gameController.buttonPressedBaia("Alimentar o Cavalo");
                gameController.baiaMinigamesCompleted = 1;
                horse.SetActive(false);
                horseInStable.SetActive(true);
                horseInStable.transform.position = positionToSpawnHorseInStable.position;
                horseInStable.transform.rotation = new Quaternion(0, 90, 0, 90);
                horseInStable.GetComponent<MoveThrowPaths>().enabled = false;
                gameController.happinnesImages.SetActive(true);
                gameController.happinnesName.SetActive(true);
                gameController.horseHappinnes.gameObject.SetActive(true);
                portaoBaia.SetActive(false);
                break;
            case "MinigameFourteen":
                gameController.buttonPressedBaia("Carinho no Cavalo");
                gameController.baiaMinigamesCompleted = 2;
                horse.SetActive(false);
                horseInStable.SetActive(true);
                horseInStable.transform.position = positionToSpawnHorseInStable.position;
                horseInStable.transform.rotation = new Quaternion(0, 90, 0, 90);
                horseInStable.GetComponent<MoveThrowPaths>().enabled = false;
                gameController.happinnesImages.SetActive(true);
                gameController.happinnesName.SetActive(true);
                gameController.horseHappinnes.gameObject.SetActive(true);
                portaoBaia.SetActive(false);
                break;
            case "MinigameFifteen":
                gameController.buttonPressedBaia("Trocar Cavalo");
                gameController.baiaMinigamesCompleted = 3;
                horse.SetActive(false);
                horseInStable.SetActive(true);
                horseInStable.transform.position = positionToSpawnHorseInStable.position;
                horseInStable.transform.rotation = new Quaternion(0, 90, 0, 90);
                horseInStable.GetComponent<MoveThrowPaths>().enabled = false;
                gameController.happinnesImages.SetActive(true);
                gameController.happinnesName.SetActive(true);
                gameController.horseHappinnes.gameObject.SetActive(true);
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
