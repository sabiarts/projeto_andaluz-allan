﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KidController : MonoBehaviour
{

    Animator kidAnim;
    public Animator horseAnim;
    public bool onHorse = true;
    float delayTimer;

    Rigidbody rb;

    private float direction = 0;
    private float xAxys;
    private float zAxys;
    public float speed;
    public float rotationSpeed;

    public bool kidInStable;

    Joystick joystick;

    public GameObject obstacleHitted;
    // Start is called before the first frame update
    void Start()
    {
        kidAnim = GetComponent<Animator>();
        joystick = FindObjectOfType<Joystick>();
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        kidAnim.SetBool("OnHorse", onHorse);
        if (onHorse)
        {
            if (horseAnim.GetBool("Walking"))
            {
                if (horseAnim.GetFloat("zAxys") != 0)
                {
                    kidAnim.SetBool("HorseWalking", true);
                    kidAnim.SetBool("HorseTroting", false);
                    kidAnim.SetBool("HorseGalloping", false);
                }
                else
                {
                    kidAnim.SetBool("HorseWalking", false);
                }
            }
            else if (horseAnim.GetBool("Troting"))
            {
                if (horseAnim.GetFloat("zAxys") != 0)
                {
                    kidAnim.SetBool("HorseWalking", false);
                    kidAnim.SetBool("HorseTroting", true);
                    kidAnim.SetBool("HorseGalloping", false);
                }
                else
                {
                    kidAnim.SetBool("HorseTroting", false);
                }
            }
            else if (horseAnim.GetBool("Galloping"))
            {
                if (horseAnim.GetFloat("zAxys") != 0)
                {
                    delayTimer += Time.deltaTime;
                    if (delayTimer > 0.2f)
                    {
                        kidAnim.SetBool("HorseWalking", false);
                        kidAnim.SetBool("HorseTroting", false);
                        kidAnim.SetBool("HorseGalloping", true);
                        delayTimer = 0;
                    }
                }
                else
                {
                    kidAnim.SetBool("HorseGalloping", false);
                }
            }
        }
        else
        {
            if (kidInStable)
            {
                zAxys = 0;
                xAxys = 0;
            }
            else
            {
                xAxys = joystick.Horizontal;
                zAxys = joystick.Vertical;
            }

            //move player
            if (zAxys > 0)
            {
                rb.velocity = transform.forward * speed;
                kidAnim.SetFloat("zAxys", zAxys);
            }
            //else if (zAxys < 0)
            //{
            //    rb.velocity = transform.forward * -speed;
            //    kidAnim.SetFloat("zAxys", zAxys);
            //}
            else
            {
                rb.velocity = Vector3.zero;
                kidAnim.SetFloat("zAxys", 0);
            }

            if (!kidInStable)
            {
                PlayerRotation();
            }

        }
    }
    public void resetHittedObject()
    {
        obstacleHitted = this.gameObject;
    }
    void PlayerRotation()
    {
        //rotate player
        if (xAxys > 0)
        {
            direction = 1;
        }
        else if (xAxys < 0)
        {
            direction = -1;
        }
        else
        {
            direction = 0;
        }
        RotatePlayer(new Vector3(0, xAxys, 0));
    }
    void RotatePlayer(Vector3 Axys)
    {
        transform.Rotate(Axys, Axys.y * rotationSpeed * direction * Time.deltaTime);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Obstacle")
        {
            obstacleHitted = other.gameObject;
            other.gameObject.SetActive(false);
        }
    }
}
