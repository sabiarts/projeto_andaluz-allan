﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HorseInStableVC : MonoBehaviour
{
    // Start is called before the first frame update
    Animator horseInStableAnim;
    void Start()
    {
        horseInStableAnim = GetComponent<Animator>();
    }

    public void changeAnim()
    {
        horseInStableAnim.SetBool("Eating", true);
    }

    public void happyAnim()
    {
        horseInStableAnim.SetBool("Happy", true);
    }

    public void resetAnim()
    {
        horseInStableAnim.SetBool("Eating", false);
        horseInStableAnim.SetBool("Happy", false);
    }
}
