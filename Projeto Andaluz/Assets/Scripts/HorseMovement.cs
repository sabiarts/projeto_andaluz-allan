﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HorseMovement : MonoBehaviour
{
    public float speed;
    public float rotationSpeed;
    private float direction = 0;
    private float xAxys;
    private float zAxys;
    public bool walkingAlone;
    public bool hitObstacle = false;
    public GameObject obstacleHitted;
    public Vector3 startPosition;
    public Quaternion startRotation;
    public Animator horseAnim;
    public bool minigameEquilibrio;

    public Slider velocitySlider;
    
    public bool stopHorse;

    private Rigidbody rb;

    Joystick joystick;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        horseAnim = GetComponent<Animator>();
        startPosition = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z);
        startRotation = new Quaternion(this.transform.rotation.x, this.transform.rotation.y, this.transform.rotation.z, this.transform.rotation.w);
        joystick = FindObjectOfType<Joystick>();
    }

    // Update is called once per frame
    void Update()
    {
        if (stopHorse)
        {
            xAxys = 0;
            zAxys = 0;
            //walkingAlone = false;
            rb.velocity = Vector3.zero;
            horseAnim.SetFloat("zAxys", 0);
            ChangeAnim();
            return;
        }

        xAxys = joystick.Horizontal;

        if (walkingAlone)
        {
            if (minigameEquilibrio)
            {
                rb.velocity = transform.forward * speed;
                horseAnim.SetBool("Walking", true);
                horseAnim.SetFloat("zAxys", 1);
                return;
            }
            rb.velocity = transform.forward * speed;
            horseAnim.SetBool("Walking", true);
            if(velocitySlider.value<= 0.5f)
            {
                horseAnim.SetFloat("zAxys", 0);
                speed = 0;
                return;
            }
            horseAnim.SetFloat("zAxys", velocitySlider.value);
            HorseRotation();
            ChangeMovementAnim();
            return;
        }
        //move horse
        if (zAxys > 0)
        {
            rb.velocity = transform.forward * speed;
            horseAnim.SetFloat("zAxys", 1);
            ChangeAnim();
        }
        else if (zAxys < 0)
        {
            rb.velocity = transform.forward * -speed;
            horseAnim.SetFloat("zAxys", -1);
            ChangeAnim();
        }
        else
        {
            rb.velocity = Vector3.zero;
            horseAnim.SetFloat("zAxys", 0);
            ChangeAnim();
        }
        HorseRotation();
    }

    public void ResetPosition()
    {
        this.transform.position = startPosition;
        this.transform.rotation = startRotation;
    }
    void HorseRotation()
    {
        //rotate horse
        if (xAxys > 0)
        {
            direction = 1;
        }
        else if (xAxys < 0)
        {
            direction = -1;
        }
        else
        {
            direction = 0;
        }
        RotateHorse(new Vector3(0, xAxys, 0));
    }
    void RotateHorse(Vector3 Axys)
    {
        transform.Rotate(Axys, Axys.y * rotationSpeed * direction * Time.deltaTime);
    }
    // Funcionts to change speed
    public void RaiseSpeed()
    {
        if (speed < 5)
        {
            speed += 2;
            rotationSpeed += 10;
            if(horseAnim.GetBool("Walking"))
            {
                horseAnim.SetBool("Walking", false);
                horseAnim.SetBool("Troting", true);
            }
            else if (horseAnim.GetBool("Troting"))
            {
                horseAnim.SetBool("Troting", false);
                horseAnim.SetBool("Galloping", true);
            }

        }  
    }

    public void LowerSpeed()
    {
        if(speed > 1)
        {
            speed -= 2;
            rotationSpeed -= 10;
            if (horseAnim.GetBool("Galloping"))
            {
                horseAnim.SetBool("Galloping", false);
                horseAnim.SetBool("Troting", true);
            }
            else if (horseAnim.GetBool("Troting"))
            {
                horseAnim.SetBool("Troting", false);
                horseAnim.SetBool("Walking", true);
            }
        }

    }

    public void ChangeMovementAnim()
    {
        if(velocitySlider.value < 0.5f)
        {
            speed = 0;
            rotationSpeed = 0;
            horseAnim.SetBool("Walking", false);
            horseAnim.SetBool("Troting", false);
            horseAnim.SetBool("Galloping", false);
        }
        else if(velocitySlider.value> 0.5f && velocitySlider.value < 1.5f)
        {
            speed = 1;
            rotationSpeed = 40;
            horseAnim.SetBool("Walking", true);
            horseAnim.SetBool("Troting", false);
            horseAnim.SetBool("Galloping", false);
        }
        else if(velocitySlider.value > 1.5f && velocitySlider.value < 2.37f)
        {
            speed = 3;
            rotationSpeed = 60;
            horseAnim.SetBool("Walking", false);
            horseAnim.SetBool("Troting", true);
            horseAnim.SetBool("Galloping", false); 
        }
        else if(velocitySlider.value > 2.37f)
        {
            speed = 5;
            rotationSpeed = 80;
            horseAnim.SetBool("Walking", false);
            horseAnim.SetBool("Troting", false);
            horseAnim.SetBool("Galloping", true);
        }
    }
    public void ChangeAnim()
    {
        if (!horseAnim.GetBool("Walking") && !horseAnim.GetBool("Troting") && !horseAnim.GetBool("Galloping"))
        {
            horseAnim.SetBool("Walking", true);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Obstacle")
        {
            hitObstacle = true;
            obstacleHitted = other.gameObject;
        }
    }
}
