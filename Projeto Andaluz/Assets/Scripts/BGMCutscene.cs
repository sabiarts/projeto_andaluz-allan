﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGMCutscene : MonoBehaviour
{
    public AudioClip music;
    AudioClip levelMusic;    
    AudioSource audioSource;


    // Start is called before the first frame update
    void Awake()
    {
        audioSource = GameObject.FindGameObjectWithTag("Bgm").GetComponent<AudioSource>(); 
    }

    void Start(){
        PlayCutsceneBgm();
    }

    public void PlayCutsceneBgm(){
        audioSource.Stop();
        levelMusic = audioSource.clip;
        Debug.Log("Stored: " + levelMusic);
        audioSource.clip = music;
        audioSource.Play();
        
    }

    public void PlayLevelBgm(){
        Debug.Log("BAW!!!");
        if(levelMusic != null){
            audioSource.Stop();
            audioSource.clip = levelMusic;
            audioSource.Play();
            Debug.Log("Level Music: " + audioSource.clip);
        }else{
            //Debug.LogError("There's no music to play in " + levelMusic.name);
        }
        
    }
    
}
