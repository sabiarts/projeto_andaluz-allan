﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveObject : MonoBehaviour
{

    public ScreenInputs screenInputsScript;
    Vector3 pointToMove;
    Collider thisCollider;
    float timeInside;
    public bool inTheRightPlace = false;
    public bool hasCollided = false;
    public bool returnToInitialPoint;
    Vector3 initialPosition;

    // Start is called before the first frame update
    void Start()
    {
        thisCollider = GetComponent<Collider>();
        initialPosition = this.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (returnToInitialPoint)
        {
            this.transform.position = Vector3.MoveTowards(this.transform.position, initialPosition, 5f);
        }
        if (screenInputsScript.objectHit.collider.name == this.gameObject.name)
        {
            pointToMove = screenInputsScript.hit.point;
            this.transform.position = pointToMove;
        }

        if (hasCollided)
        {
            timeInside += Time.deltaTime;
            if(timeInside > .5f)
            {
                inTheRightPlace = true;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "PlaceToPut")
        {
            hasCollided = true;
        }

    }
}
