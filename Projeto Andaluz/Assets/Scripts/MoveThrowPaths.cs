﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveThrowPaths : MonoBehaviour
{
    public int speed;
    public bool selectedTheRightPlace;
    public Transform[] path;
    public Transform enterStable;
    public Transform lookInStable;
    private bool inStable;
    public int pointToGo = 0;
    private ScreenInputs screenInputsScript;
    public bool arrivedInLocation = false;
    private Animator horseInStableAnim;
    float timeToRotate;

    public GameController gameController;


    public bool finishedWalkingAnimation = false;
    // Start is called before the first frame update
    void Start()
    {
        screenInputsScript = GameObject.FindGameObjectWithTag("ScreenInputs").GetComponent<ScreenInputs>();
        horseInStableAnim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        float step = Time.deltaTime * speed;
        if (!inStable)
        {
            float distanciaStable = Vector3.Distance(this.transform.position, enterStable.position);
            this.transform.position = Vector3.MoveTowards(this.transform.position, enterStable.position, step);
            horseInStableAnim.SetBool("Walking", true);
            if (this.gameObject.name != "criançaFinal")
                horseInStableAnim.SetFloat("zAxys", 1);
            if (distanciaStable < 0.5f)
            {
                horseInStableAnim.SetBool("Walking", false);
                horseInStableAnim.SetFloat("zAxys", 0);
                inStable = true;
            }
            return;
        }
        if (screenInputsScript.objectHit.collider.tag == "rightPlaceClicked" && !selectedTheRightPlace)
        {
            if (gameController.callOneTime)
            {
                gameController.soundEffectController.PlayAudio("Criancas_Yay_Curto", true); 
                gameController.callOneTime = false;
            }
            screenInputsScript.objectHit.collider.gameObject.GetComponent<MeshRenderer>().enabled = false;
            screenInputsScript.objectHit.collider.enabled = false;
            screenInputsScript.enabled = false;
            selectedTheRightPlace = true;
        }else if(screenInputsScript.objectHit.collider.tag != "rightPlaceClicked" && !selectedTheRightPlace)
        {
            gameController.soundEffectController.PlayAudio("Cavalo_som_negativo", true);
        }
        if (selectedTheRightPlace)
        {
            horseInStableAnim.SetBool("Walking", true);
            if (this.gameObject.name != "criançaFinal")
                horseInStableAnim.SetFloat("zAxys", 1);
            
            if (pointToGo < path.Length)
            {
                this.transform.position = Vector3.MoveTowards(this.transform.position, path[pointToGo].transform.position, step);
            }
            else
            {
                arrivedInLocation = true;
                timeToRotate += Time.deltaTime;
                if (timeToRotate > 3.5f)
                {
                    horseInStableAnim.SetBool("Walking", false);
                    horseInStableAnim.SetFloat("zAxys", 0);
                    finishedWalkingAnimation = true;
                    screenInputsScript.enabled = true;
                }
            }
            float distancia = Vector3.Distance(this.transform.position, path[pointToGo].transform.position);
            if (distancia < 0.5f)
            {
                pointToGo++;
            }
        }
    }
    private void FixedUpdate()
    {
        if (arrivedInLocation && this.gameObject.name != "criançaFinal")
        {
            this.transform.rotation = Quaternion.RotateTowards(this.transform.rotation, lookInStable.rotation, 1f);
        }
        else
        {
            this.transform.rotation = Quaternion.RotateTowards(this.transform.rotation, path[pointToGo].rotation, 1f);
        }
    }
}
