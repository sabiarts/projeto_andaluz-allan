﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndGameTrigger : MonoBehaviour
{
    GameController gameController;
    

    private void Start()
    {
        gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if(this.tag == "EndMinigamePlace" && gameController.setOneTime)
        {
            gameController.setDicasWin();
            gameController.soundEffectController.PlayAudio("Criancas_Yay_Curto", true);
            gameController.setWinMinigameParticles();
        }
        else if(this.tag == "Lose" && gameController.setOneTime)
        {
            gameController.setDicasLose();
            gameController.soundEffectController.PlayAudio("Bruhh_1", true);
            gameController.setLoseMinigameParticles();
        }
    }

}
