﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{

    public SoundEffectController soundEffectController;
    public SoundEffectController effectController;
    public Texture2D cursor;
    public GameObject cursorImage;
    public GameObject maozinhaBaia;
    [Header("Cutscenes")]
    public bool finishedCutScene;
    public bool finishedDicas;
    public GameObject baia1Cutscene;
    public GameObject baia2Cutscene;
    public GameObject baia3Cutscene;
    public GameObject baia4Cutscene;
    public GameObject baia5Cutscene;
    public GameObject Equoterapia1Cutscene;
    public GameObject Equoterapia2Cutscene;
    public GameObject Equoterapia3Cutscene;
    public GameObject Equoterapia4Cutscene;
    public GameObject Pre1Cutscene;
    public GameObject Pre2Cutscene;
    public GameObject Pre3Cutscene;
    public GameObject Pre4Cutscene;
    public GameObject Pre5Cutscene;
    public GameObject EndCutscene;


    public AxysCorrection axysCorrection;
    private string lastSceneLoaded;
    public string sceneLoaded;
    public MoveObject[] moveObjectsScript;
    public GameObject[] CollidersMinigameTwo;
    public GameObject[] checkPointsMinigameNine;
    public GameObject[] lettersUIMinigameTen;
    public GameObject[] lettersUIMinigameTenBackground;
    public GameObject[] objectsMinigameOne;
    public GameObject[] objectsMinigameOneUI;
    public GameObject[] objectsMinigameOneUIBackground;
    public bool InHistory;

    private int collectedLetters;
    private int collectedObjects;
    public float timeAttack;
    public int timeAttackInt;
    public GameObject timeAttackTitle;
    public Text timeAttackText;
    public HorseMovement horseMovement;
    public MoveThrowPaths moveThrowPaths;
    public ScreenInputs screenInputs;
    public ScreenInputs screenInputs2;
    public ScreenInputs screenInputs3;
    public GameObject HorseInStable;

    public GameObject bandeiras1;
    public GameObject bandeiras2;

    public GameObject KidInHorseStable;

    public KidController kidController;

    public PointsController pointsControllerScript;

    [Header("Canvas Buttons")]
    public GameObject velocityController;
    public GameObject joystick;

    [Header("Cameras")]
    public GameObject camera2;
    public GameObject camera3;
    public GameObject camera4;
    public GameObject camera5;
    public GameObject camera6;
    public GameObject camera7;

    public HorseInStableVC horseInStableVC;
    public Text MinigameName;
    public Text voltasText;
    public GameObject AtividadesEmBaiaButtons;
    public GameObject BotoesMinigamesBaia;
    public GameObject AlimentarCavaloButoons;
    public GameObject[] horseInStableParticles;
    public bool finishedBaiaMinigame;
    private bool horseDeactivated;
    public FoodColission foodColission;

    public Renderer horseInStableMaterialObject;
    public Renderer horseMaterialObject;
    public Material[] horseAndaluzMaterial;
    public GameObject changeHorseCanvas;
    bool changedHorse;
    public Slider horseHappinnes;
    public GameObject happinnesName;
    public GameObject happinnesImages;
    public int baiaMinigamesCompleted;

    public GameObject carregandoEndUI;
    public GameObject celaCavalo;
    public GameObject mantaCavalo;

    public Animator dicasAnim;
    public GameObject dicasWin;
    public GameObject dicasLose;

    public GameObject dicasEscovando;
    public GameObject dicasAlimentando;
    public GameObject dicasCarinho;
    public GameObject dicasTrocar;

    //Unique Variables
    private float timeCleaning = 0;
    private float timeLookingForToys;
    public bool startGame;
    private string checkPointToBeReached = "checkPoint" + 1;
    private int checkPointNumber = 1;
    private int numberOfLaps = 0;
    private bool dicasUnset;

    public bool callOneTime = true;
    public bool setOneTime;
    public bool endedWinOrLose;
    public AudioSource AudioSourceBGM;
    public AudioSource AudioSourceSE;


    // Start is called before the first frame update
    void Start()
    {
        if (startGame)
        {
            pointsControllerScript.ResetAllThePoints();
            startGame = false;
            InHistory = true;
            if(PlayerPrefs.GetString("HorseChoosed").Length != 0)
            {
                changeHorse(PlayerPrefs.GetString("HorseChoosed"));
            }
            else
            {
                changeHorse("Titan");
            }

        }
        Debug.Log(PlayerPrefs.GetString("HorseChoosed"));
        changeHorse(PlayerPrefs.GetString("HorseChoosed"));
        sceneLoaded = PlayerPrefs.GetString("MinigameAt");
        if(GameObject.FindGameObjectWithTag("Horse") != null)
        {
            horseMovement = GameObject.FindGameObjectWithTag("Horse").GetComponent<HorseMovement>();
        }
        checkPointToBeReached = "checkPoint" + checkPointNumber;

        getVolumes();
    }

    // Update is called once per frame
    void Update()
    {
        switch (sceneLoaded){
            case "MinigameOne":
                MinigameOne();
                break;
            case "MinigameTwo":
                MinigameTwo();
                break;
            case "MinigameThree":
                MinigameThree();
                break;
            case "MinigameFour":
                MinigameFour();
                break;
            case "MinigameFive":
                MinigameFive();
                break;
            case "MinigameSix":
                MinigameSix();
                break;
            case "MinigameSeven":
                MinigameSeven();
                break;
            case "MinigameEight":
                MinigameEight();
                break;
            case "MinigameNine":
                MinigameNine();
                break;
            case "MinigameNine2":
                MinigameNine2();
                break;
            case "MinigameTen":
                MinigameTen();
                break;
            case "MinigameEleven":
                MinigameEleven();
                break;
            case "MinigameTwelve":
                MinigameTwelve();
                break;
            case "MinigameThirteen":
                MinigameThirteen();
                break;
            case "MinigameFourteen":
                MinigameFourteen();
                break;
            case "MinigameFifteen":
                MinigameFifteen();
                break;
            case "BaiaTransitions":
                BaiaTransitions();
                break;
        }
        PlayerPrefs.SetString("MinigameAt", sceneLoaded);
    }

    private void getVolumes()
    {
        AudioSourceBGM.volume = (PlayerPrefs.GetFloat("MusicaVolume", 1) * PlayerPrefs.GetFloat("GeralVolume", 1));
        AudioSourceSE.volume = (PlayerPrefs.GetFloat("EfeitoVolume", 1) * PlayerPrefs.GetFloat("GeralVolume", 1));
    }

    public void setParticles()
    {
        horseInStableParticles[0].SetActive(true);
        horseInStableParticles[0].transform.position = kidController.transform.position;
        StartCoroutine(resetParticle());
    }
    public void setWinMinigameParticles()
    {
        horseInStableParticles[1].SetActive(true);
        horseInStableParticles[1].transform.position = kidController.transform.position;
        StartCoroutine(resetParticle());
    }
    public void setLoseMinigameParticles()
    {
        horseInStableParticles[2].SetActive(true);
        if(sceneLoaded != "MinigameFour")
        {
            horseInStableParticles[2].transform.position = kidController.transform.position;
        }        
        StartCoroutine(resetParticle());
    }
    void MinigameOne()
    {
        if (!finishedCutScene)
        {
            return;
        }
        if (callOneTime)
        {
            soundEffectController.PlayAudio("Som_Ambiente_Fora", true);
            callOneTime = false;
        }
        Equoterapia4Cutscene.SetActive(false);
        MinigameName.text = "Buscando os brinquedos.";
        if (timeLookingForToys > 120f && setOneTime)
        {
            setDicasLose();
            setOneTime = false;
        }
        else
        {
            timeLookingForToys += Time.deltaTime;
        }
        switch (kidController.obstacleHitted.name)
        {
            case "Kek":
                objectsMinigameOne[0].SetActive(false);
                objectsMinigameOneUI[0].SetActive(true);
                objectsMinigameOneUIBackground[0].SetActive(false);
                setParticles();
                pointsControllerScript.AddPoints(500);
                effectController.PlayAudio("Criancas_Yay_Curto", true);
                kidController.resetHittedObject();
                break;
            case "Bola":
                objectsMinigameOne[1].SetActive(false);
                objectsMinigameOneUI[1].SetActive(true);
                objectsMinigameOneUIBackground[1].SetActive(false);
                setParticles();
                pointsControllerScript.AddPoints(500);
                effectController.PlayAudio("Criancas_Yay_Curto", true);
                kidController.resetHittedObject();
                break;
            case "Urso":
                objectsMinigameOne[2].SetActive(false);
                objectsMinigameOneUI[2].SetActive(true);
                objectsMinigameOneUIBackground[2].SetActive(false);
                setParticles();
                pointsControllerScript.AddPoints(500);
                effectController.PlayAudio("Criancas_Yay_Curto", true);
                kidController.resetHittedObject();
                break;
        }
        collectedObjects = 0;
        for (int i = 0; i < objectsMinigameOneUI.Length; i++)
        {
            if (objectsMinigameOneUI[i].activeSelf)
            {
                collectedObjects++;
            }
        }
        Debug.Log(collectedObjects);
        if (collectedObjects == 3)
        {
            setDicasWin();
        }
    }

    void MinigameTwo()
    {
        horseMovement.walkingAlone = true;
        if (!finishedCutScene)
        {
            Equoterapia1Cutscene.SetActive(true);
            horseMovement.stopHorse = true;
            return;
        }
        MinigameName.text = "Acertando os Brinquedos";
        Equoterapia1Cutscene.SetActive(false);
        horseMovement.stopHorse = false;
        if (horseMovement.hitObstacle)
        {
            horseMovement.stopHorse = true;
        }

        if (moveObjectsScript[0].inTheRightPlace)
        {
            setDicasWin();

            screenInputs.gameObject.SetActive(false);
            pointsControllerScript.AddPoints(100);
            moveObjectsScript[0].inTheRightPlace = false;
            moveObjectsScript[0].hasCollided = false;
            CollidersMinigameTwo[0].SetActive(false);


            moveObjectsScript[0].enabled = false;
            moveObjectsScript[1].enabled = true;
            moveObjectsScript[2].enabled = false;

            horseMovement.hitObstacle = false;
            horseMovement.stopHorse = false;
            horseMovement.walkingAlone = true;
            screenInputs2.gameObject.SetActive(true);
            soundEffectController.PlayAudio("Criancas_Yay_Curto", true);
            setParticles();
        }
        else if (moveObjectsScript[1].inTheRightPlace)
        {
            setDicasWin();

            screenInputs2.gameObject.SetActive(false);
            pointsControllerScript.AddPoints(100);
            CollidersMinigameTwo[1].SetActive(false);
            moveObjectsScript[1].inTheRightPlace = false;
            moveObjectsScript[1].hasCollided = false;

            moveObjectsScript[0].enabled = false;
            moveObjectsScript[1].enabled = false;
            moveObjectsScript[2].enabled = true;

            horseMovement.hitObstacle = false;
            horseMovement.stopHorse = false;
            horseMovement.walkingAlone = true;
            screenInputs3.gameObject.SetActive(true);
            soundEffectController.PlayAudio("Criancas_Yay_Curto", true);
            setParticles();
        }
        else if (moveObjectsScript[2].inTheRightPlace)
        {
            pointsControllerScript.AddPoints(100);
            CollidersMinigameTwo[2].SetActive(false);
            moveObjectsScript[2].inTheRightPlace = false;
            moveObjectsScript[2].hasCollided = false;
            soundEffectController.PlayAudio("Criancas_Yay_Curto", true);
            setParticles();
            setDicasLose();

        }
    }
    void MinigameThree()
    {
        if (!finishedCutScene)
        {
            Equoterapia2Cutscene.SetActive(true);
            horseMovement.stopHorse = true;
            return;
        }
        MinigameName.text = "Se equilibrando!";
        Equoterapia2Cutscene.SetActive(false);
        horseMovement.walkingAlone = true;
        //horseMovement.stopHorse = false;
        if (dicasUnset)
        {
            axysCorrection.enabled = true;
        }
        else
        {
            axysCorrection.enabled = false;
        }

    }

    void MinigameFour()
    {
        if (!finishedCutScene)
        {
            Equoterapia3Cutscene.SetActive(true);
            return;
        }
        MinigameName.text = "Ritmo!";
        Equoterapia3Cutscene.SetActive(false);
    }

    void MinigameFive()
    {
        if (!finishedCutScene)
        {
            Pre4Cutscene.SetActive(true);
            horseHappinnes.value = 1f;
            return;
        }
        MinigameName.text = "Zig Zag!";
        Pre4Cutscene.SetActive(false);
        horseMovement.walkingAlone = true;
        horseMovement.speed = 1;
    }

    void MinigameSix()
    {
        if (!finishedCutScene)
        {
            Pre3Cutscene.SetActive(true);
            horseHappinnes.value = 1f;
            horseMovement.walkingAlone = false;
            horseMovement.stopHorse = true;
            return;
        }
        timeAttackTitle.SetActive(true);
        if (dicasUnset)
        {
            timeAttack -= Time.deltaTime;
            timeAttackInt = (int)timeAttack;
            timeAttackText.text = timeAttackInt.ToString();
            if (timeAttack <= 0)
            {
                timeAttackText.text = "0";
                SceneManager.LoadScene(sceneLoaded);
            }
        }
        MinigameName.text = "Zig Zag com Velocidade!";
        velocityController.SetActive(true);
        Pre3Cutscene.SetActive(false);
        horseMovement.walkingAlone = true;
        //horseMovement.stopHorse = false;
        bandeiras1.SetActive(true);
        bandeiras2.SetActive(false);
    }

    void MinigameSeven()
    {
        if (!finishedCutScene)
        {
            Pre1Cutscene.SetActive(true);
            horseHappinnes.value = 1f;
            horseMovement.walkingAlone = false;
            horseMovement.stopHorse = true;
            return;
        }
        MinigameName.text = "Mais rápido!";
        velocityController.SetActive(true);
        horseMovement.walkingAlone = true;
        //horseMovement.stopHorse = false;
        Pre1Cutscene.SetActive(false);
        bandeiras1.SetActive(false);
        bandeiras2.SetActive(true);
        timeAttackText.gameObject.SetActive(true);
        timeAttackTitle.SetActive(true);
        if (dicasUnset)
        {
            timeAttack -= Time.deltaTime;
            timeAttackInt = (int)timeAttack;
            timeAttackText.text = timeAttackInt.ToString();
            if (timeAttack <= 0)
            {
                timeAttackText.text = "0";
                SceneManager.LoadScene(sceneLoaded);
            }
        }
        if (horseMovement.hitObstacle)
        {
            EndMiniGame();
        }
    }

    void MinigameEight()
    {
        if (!finishedCutScene)
        {
            Pre2Cutscene.SetActive(true);
            horseHappinnes.value = 1f;
            horseMovement.walkingAlone = false;
            horseMovement.stopHorse = true;
            return;
        }
        MinigameName.text = "Zig Zag com tempo.";
        velocityController.SetActive(true);
        timeAttackText.gameObject.SetActive(true);
        horseMovement.walkingAlone = true;
        //horseMovement.stopHorse = false;
        bandeiras1.SetActive(false);
        bandeiras2.SetActive(true);
        Pre2Cutscene.SetActive(false);
        timeAttack -= Time.deltaTime;
        timeAttackInt = (int)timeAttack;
        timeAttackText.text = timeAttackInt.ToString();
        if (timeAttack <= 0)
        {
            timeAttackText.text = "0";
            EndMiniGame();
        }
        if (horseMovement.hitObstacle)
        {
            EndMiniGame();
        }
    }
    void MinigameNine()
    {
        if (!finishedCutScene)
        {
            Pre4Cutscene.SetActive(true);
            horseMovement.walkingAlone = false;
            return;
        }
        MinigameName.text = "Pista completa!";
        horseMovement.walkingAlone = true;
        //horseMovement.stopHorse = false;
        Pre4Cutscene.SetActive(false);
        if (dicasUnset)
        {
            timeAttack -= Time.deltaTime;
            timeAttackInt = (int)timeAttack;
            timeAttackText.text = timeAttackInt.ToString();
            if (timeAttack <= 0)
            {
                timeAttackText.text = "0";
                SceneManager.LoadScene(sceneLoaded);
            }
        }
        if (horseMovement.obstacleHitted.name == checkPointToBeReached)
        {
            soundEffectController.PlayAudio("Criancas_Yay_Curto", true);
            setParticles();
            pointsControllerScript.AddPoints(100);
            if (checkPointNumber == 8)
            {
                numberOfLaps += 1;
                voltasText.text = numberOfLaps + "/2";
                checkPointNumber = 1;
                checkPointToBeReached = "checkPoint" + checkPointNumber;
            }
            else
            {
                checkPointNumber++;
                checkPointToBeReached = "checkPoint" + checkPointNumber;
            }

        }
        if(numberOfLaps == 2)
        {
            setDicasWin();
        }
    }

    public void MinigameNine2()
    {
        if (!finishedCutScene)
        {
            Pre5Cutscene.SetActive(true);
            horseMovement.walkingAlone = false;
            return;
        }
        MinigameName.text = "Duas Voltas!";
        horseMovement.walkingAlone = true;
        //horseMovement.stopHorse = false;
        Pre5Cutscene.SetActive(false);
        if (dicasUnset)
        {
            timeAttack -= Time.deltaTime;
            timeAttackInt = (int)timeAttack;
            timeAttackText.text = timeAttackInt.ToString();
            if (timeAttack <= 0)
            {
                timeAttackText.text = "0";
                SceneManager.LoadScene(sceneLoaded);
            }
        }
        if (horseMovement.obstacleHitted.name == checkPointToBeReached && horseMovement.obstacleHitted != null)
        {
            soundEffectController.PlayAudio("Criancas_Yay_Curto", true);
            pointsControllerScript.AddPoints(100);
            setParticles();
            if (checkPointNumber == 8)
            {
                numberOfLaps += 1;
                voltasText.text = numberOfLaps + "/2";
                checkPointNumber = 1;
                checkPointToBeReached = "checkPoint" + checkPointNumber;
            }
            else
            {
                checkPointNumber++;
                checkPointToBeReached = "checkPoint" + checkPointNumber;
            }

        }
        if (numberOfLaps == 2)
        {
            setDicasWin();
        }
    }
    void MinigameTen()
    {
        if (!finishedCutScene)
        {
            return;
        }
        if (callOneTime)
        {
            soundEffectController.PlayAudio("Som_Ambiente_Fora", true);
            callOneTime = false;
        }
        timeAttack -= Time.deltaTime;
        timeAttackInt = (int)timeAttack;
        timeAttackText.text = timeAttackInt.ToString();
        if (timeAttack <= 0)
        {
            timeAttackText.text = "0";
            SceneManager.LoadScene(sceneLoaded);
        }
        MinigameName.text = "Buscando as Letras!";
        Equoterapia4Cutscene.SetActive(false);
        switch (kidController.obstacleHitted.name)
        {
            case "A":
                if (!lettersUIMinigameTen[0].activeSelf)
                {
                    lettersUIMinigameTen[0].SetActive(true);
                    pointsControllerScript.AddPoints(200);
                    effectController.PlayAudio("Criancas_Yay_Curto", true);
                    setParticles();
                }
                break;
            case "N":
                if (!lettersUIMinigameTen[1].activeSelf)
                {
                    lettersUIMinigameTen[1].SetActive(true);
                    lettersUIMinigameTenBackground[1].SetActive(false);
                    pointsControllerScript.AddPoints(200);
                    effectController.PlayAudio("Criancas_Yay_Curto", true);
                    setParticles();
                }
                    
                break;
            case "D":
                if (!lettersUIMinigameTen[2].activeSelf)
                {
                    lettersUIMinigameTen[2].SetActive(true);
                    lettersUIMinigameTenBackground[2].SetActive(false);
                    pointsControllerScript.AddPoints(200);
                    effectController.PlayAudio("Criancas_Yay_Curto", true);
                    setParticles();
                }
                    
                break;
            case "a":
                if (!lettersUIMinigameTen[3].activeSelf)
                {
                    lettersUIMinigameTen[3].SetActive(true);
                    lettersUIMinigameTenBackground[3].SetActive(false);
                    pointsControllerScript.AddPoints(200);
                    effectController.PlayAudio("Criancas_Yay_Curto", true);
                    setParticles();
                }
                break;
            case "L":
                if (!lettersUIMinigameTen[4].activeSelf)
                {
                    lettersUIMinigameTen[4].SetActive(true);
                    lettersUIMinigameTenBackground[4].SetActive(false);
                    pointsControllerScript.AddPoints(200);
                    effectController.PlayAudio("Criancas_Yay_Curto", true);
                    setParticles();
                }
                break;
            case "U":
                if (!lettersUIMinigameTen[5].activeSelf)
                {
                    lettersUIMinigameTen[5].SetActive(true);
                    lettersUIMinigameTenBackground[5].SetActive(false);
                    pointsControllerScript.AddPoints(200);
                    effectController.PlayAudio("Criancas_Yay_Curto", true);
                    setParticles();
                }
                break;
            case "Z":
                if (!lettersUIMinigameTen[6].activeSelf)
                {
                    lettersUIMinigameTen[6].SetActive(true);
                    lettersUIMinigameTenBackground[6].SetActive(false);
                    pointsControllerScript.AddPoints(200);
                    effectController.PlayAudio("Criancas_Yay_Curto", true);
                    setParticles();
                }
                break;
        }
        collectedLetters = 0;
        for (int i = 0; i < lettersUIMinigameTen.Length; i++)
        {
            if (lettersUIMinigameTen[i].activeSelf)
            {
                collectedLetters++;
            }
        }
        Debug.Log(collectedLetters);
        if (collectedLetters == 7)
        {
            setDicasWin();
        }
    }

    void MinigameEleven()
    {
        if (!finishedCutScene)
        {
            baia1Cutscene.SetActive(true);
            horseMovement.speed = 0;
            return;
        }
        horseMovement.speed = 5;
        baia1Cutscene.SetActive(false);
        if (!horseDeactivated)
        {
            horseMovement = GameObject.FindGameObjectWithTag("Horse").GetComponent<HorseMovement>();
        }
        if (horseMovement.hitObstacle)
        {
            horseDeactivated = true;
            HorseInStable.SetActive(true);
            KidInHorseStable.SetActive(true);
            horseMovement.gameObject.SetActive(false);
            if (moveThrowPaths.selectedTheRightPlace)
            {
                maozinhaBaia.SetActive(false);
            }
            else
            {
                maozinhaBaia.SetActive(true);
            }
            camera2.SetActive(true);
            joystick.SetActive(false);
        }
        if (moveThrowPaths.arrivedInLocation)
        {
            camera2.SetActive(false);
            camera3.SetActive(true);

        }
        if (moveThrowPaths.finishedWalkingAnimation)
        {
            camera3.SetActive(false);
            camera4.SetActive(true);
            KidInHorseStable.SetActive(false);
            AtividadesEmBaiaButtons.SetActive(true);
            moveThrowPaths.enabled = false;
            sceneLoaded = "BaiaTransitions";
            if (InHistory)
            {
                finishedBaiaMinigame = true;
            }
        }
    }

    void BaiaTransitions()
    {
        if (finishedBaiaMinigame)
        {
            camera4.SetActive(true);
            camera5.SetActive(false);
            camera6.SetActive(false);
            camera7.SetActive(false);
            changeHorseCanvas.SetActive(false);
            AtividadesEmBaiaButtons.SetActive(true);
            BotoesMinigamesBaia.SetActive(true);
            happinnesImages.SetActive(true);
            happinnesName.SetActive(true);
            horseHappinnes.gameObject.SetActive(true);
            baiaMinigamesCompleted++;
            finishedBaiaMinigame = false;
            if (InHistory)
            {
                switch (lastSceneLoaded)
                {
                    case null:
                        buttonPressedBaia("Escovar o Cavalo");
                        break;
                    case "MinigameTwelve":
                        buttonPressedBaia("Alimentar o Cavalo");
                        break;
                    case "MinigameThirteen":
                        buttonPressedBaia("Carinho no Cavalo");
                        break;
                    case "MinigameFourteen":
                        buttonPressedBaia("Trocar Cavalo");
                        break;
                    case "MinigameFifteen":
                        EndMiniGame();
                        break;
                }
            }
        }
        if(baiaMinigamesCompleted >3)
        {
            EndMiniGame();
        }
        Debug.Log(baiaMinigamesCompleted);
    }
    public void buttonPressedBaia(string buttonName)
    {
        //AtividadesEmBaiaButtons.SetActive(false);
        switch (buttonName)
        {
            case "Escovar o Cavalo":
                camera4.SetActive(false);
                camera5.SetActive(true);
                finishedCutScene = false;
                MinigameName.text = "Escovando o Titan.";
                changeDicas(dicasEscovando);
                celaCavalo.SetActive(false);
                mantaCavalo.SetActive(false);
                baia2Cutscene.SetActive(true);
                sceneLoaded = "MinigameTwelve";
                BotoesMinigamesBaia.SetActive(false);
                horseHappinnes.value = 50;
                break;
            case "Alimentar o Cavalo":
                camera4.SetActive(false);
                camera6.SetActive(true);
                finishedCutScene = false;
                MinigameName.text = "Hora do Lanche!";
                changeDicas(dicasAlimentando);
                celaCavalo.SetActive(true);
                mantaCavalo.SetActive(true);
                baia3Cutscene.SetActive(true);
                sceneLoaded = "MinigameThirteen";
                BotoesMinigamesBaia.SetActive(false);
                horseHappinnes.value = 50;
                break;
            case "Carinho no Cavalo":
                camera4.SetActive(false);
                camera7.SetActive(true);
                finishedCutScene = false;
                baia5Cutscene.SetActive(true);
                MinigameName.text = "Eles também gostam de carinho!";
                changeDicas(dicasCarinho);
                sceneLoaded = "MinigameFourteen";
                BotoesMinigamesBaia.SetActive(false);
                horseHappinnes.value = 50;
                break;
            case "Trocar Cavalo":
                camera4.SetActive(false);
                camera7.SetActive(true);
                finishedCutScene = false;
                baia4Cutscene.SetActive(true);
                MinigameName.text = "Com quem vamos passear?";
                changeDicas(dicasTrocar);
                changeHorseCanvas.SetActive(true);
                sceneLoaded = "MinigameFifteen";
                BotoesMinigamesBaia.SetActive(false);
                happinnesImages.SetActive(false);
                happinnesName.SetActive(false);
                horseHappinnes.gameObject.SetActive(false);
                break;
        }
    }

    void MinigameTwelve()
    {
        if (!finishedCutScene)
        {
            return;
        }
        baia2Cutscene.SetActive(false);
        if (screenInputs.objectHit.collider.tag == "HorseInStable" && screenInputs.movingFinger)
        {
            //start visual effects and count time doing it
            horseHappinnes.value += Time.deltaTime * 10;
            //Cursor.SetCursor(cursor, Vector2.zero, CursorMode.Auto);
            cursorImage.SetActive(true);
            cursorImage.transform.position = screenInputs.objectHit.point;
            horseInStableParticles[1].SetActive(true);
            horseInStableParticles[1].transform.position = screenInputs.hit.point;
            if (callOneTime)
            {
                soundEffectController.PlayAudio("Escovada 1", false);
                callOneTime = false;
            }
            horseInStableParticles[0].SetActive(true);
            horseInStableParticles[0].transform.position = HorseInStable.transform.position;
            horseInStableVC.happyAnim();
            if (horseHappinnes.value >90)
            {
                StartCoroutine(resetParticleHorseStable());
                horseInStableVC.resetAnim();
                cursorImage.SetActive(false);
                lastSceneLoaded = sceneLoaded;
                soundEffectController.PlayAudio("Relincho_médio", true);
                setDicasWin();
            }
        }

        else if(!screenInputs.fingerOnScreen && horseHappinnes.value < 90)
        {
            callOneTime = true;
            StartCoroutine(resetParticleHorseStableFast());
            soundEffectController.StopAudio();
            horseInStableParticles[1].SetActive(false);
            horseHappinnes.value -= Time.deltaTime * 2;
            horseInStableVC.resetAnim();
        }
        if (endedWinOrLose && setOneTime)
        {
            //Congrats Screen
            unsetDicas();
            soundEffectController.StopAudio();
            horseInStableParticles[1].SetActive(false);
            horseInStableParticles[1].SetActive(false);
            lastSceneLoaded = sceneLoaded;
            carregandoEndUI.SetActive(true);
            StartCoroutine(CarregandoEndSameScene());
            sceneLoaded = "BaiaTransitions";
            finishedBaiaMinigame = true;
            endedWinOrLose = false;
            horseHappinnes.value = 40;
            Time.timeScale = 1;
            Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
        }
    }

    void MinigameThirteen()
    {
        if (!finishedCutScene)
        {
            Time.timeScale = 1;
            return;
        }
        baia3Cutscene.SetActive(false);
        AlimentarCavaloButoons.SetActive(true);
        changeHorseCanvas.SetActive(false);
        if(horseHappinnes.value > 80)
        {
            setDicasWin();
            lastSceneLoaded = sceneLoaded;
            soundEffectController.StopAudio();
        }
        else
        {
            horseHappinnes.value -= Time.deltaTime * 2;
        }
        if (foodColission.collidedWithCenoura)
        {
            soundEffectController.PlayAudio("Cavalo_Comendo2", true);
            horseInStableParticles[3].transform.position = foodColission.transform.position;
            horseInStableParticles[3].SetActive(true);
            horseInStableVC.changeAnim();
            StartCoroutine(resetParticleHorseStable());
            foodColission.collidedWithCenoura = false;
            horseHappinnes.value += 20;
        }
        else if (foodColission.collidedWithMaca)
        {
            soundEffectController.PlayAudio("Cavalo_Comendo2", true);
            horseInStableParticles[2].transform.position = foodColission.transform.position;
            horseInStableParticles[2].SetActive(true);
            horseInStableVC.changeAnim();
            StartCoroutine(resetParticleHorseStable());
            foodColission.collidedWithMaca = false;
            horseHappinnes.value += 20;
        }
        else if (foodColission.collidedWithFeno)
        {
            soundEffectController.PlayAudio("Cavalo_Comendo2", true);
            horseInStableParticles[4].transform.position = foodColission.transform.position;
            horseInStableParticles[4].SetActive(true);
            horseInStableVC.changeAnim();
            StartCoroutine(resetParticleHorseStable());
            foodColission.collidedWithFeno = false;
            horseHappinnes.value += 20;
        }
    }

    IEnumerator resetParticleHorseStableFast()
    {
        yield return new WaitForSeconds(1f);
        for (int i = 0; i < horseInStableParticles.Length; i++)
        {
            horseInStableParticles[i].SetActive(false);
        }
    }

    IEnumerator resetParticleHorseStable()
    {
        yield return new WaitForSeconds(3f);
        for (int i = 0; i < horseInStableParticles.Length; i++)
        {
            horseInStableParticles[i].SetActive(false);
        }
        horseInStableVC.resetAnim();

    }
    IEnumerator resetParticle()
    {
        yield return new WaitForSeconds(3f);
        for (int i = 0; i < horseInStableParticles.Length; i++)
        {
            horseInStableParticles[i].SetActive(false);
        }

    }


    void MinigameFourteen()
    {
        if (!finishedCutScene)
        {
            return;
        }
        baia5Cutscene.SetActive(false);
        if (screenInputs.objectHit.collider.tag == "HorseInStable" && screenInputs.movingFinger)
        {
            if (callOneTime)
            {
                soundEffectController.PlayAudio("Escovada 2", false);
                callOneTime = false;
            }
            //start visual effects and count time doing it
            horseInStableVC.happyAnim();
            horseHappinnes.value += Time.deltaTime * 10;
            horseInStableParticles[0].SetActive(true);
            horseInStableParticles[0].transform.position = HorseInStable.transform.position;
            if (horseHappinnes.value > 90)
            {
                //Congrats Screen
                setDicasWin();
                horseInStableVC.resetAnim();
                horseInStableParticles[0].SetActive(false);
                lastSceneLoaded = sceneLoaded;
                soundEffectController.PlayAudio("Relincho_médio", true);
            }
        }
        else if (!screenInputs.fingerOnScreen && horseHappinnes.value < 90)
        {
            horseInStableVC.resetAnim();
            horseInStableParticles[1].SetActive(false);
            horseHappinnes.value -= Time.deltaTime * 2;
            callOneTime = true;
            soundEffectController.StopAudio();
        }
    }

    void MinigameFifteen()
    {
        if (!finishedCutScene)
        {
            changedHorse = false;
            return;
        }
        baia4Cutscene.SetActive(false);
        if (changedHorse)
        {
            horseInStableVC.happyAnim();
            StartCoroutine(resetParticleHorseStable());
            lastSceneLoaded = sceneLoaded;
            sceneLoaded = "BaiaTransitions";
            finishedBaiaMinigame = true;
            changedHorse = false;
        }
    }
    public void changeHorse(string Horse)
    {
        if(sceneLoaded == "MinigameFifteen")
        {
            soundEffectController.PlayAudio("Relincho_médio", true);
        }
        switch (Horse)
        {
            case "Belinha":
                horseInStableMaterialObject.sharedMaterial = horseAndaluzMaterial[0];
                horseMaterialObject.sharedMaterial = horseAndaluzMaterial[0];
                break;
            case "Bill":
                horseInStableMaterialObject.sharedMaterial = horseAndaluzMaterial[1];
                horseMaterialObject.sharedMaterial = horseAndaluzMaterial[1];
                break;
            case "Drago":
                horseInStableMaterialObject.sharedMaterial = horseAndaluzMaterial[2];
                horseMaterialObject.sharedMaterial = horseAndaluzMaterial[2];
                break;
            case "Titan":
                horseInStableMaterialObject.sharedMaterial = horseAndaluzMaterial[3];
                horseMaterialObject.sharedMaterial = horseAndaluzMaterial[3];
                break;
            case "Max":
                horseInStableMaterialObject.sharedMaterial = horseAndaluzMaterial[4];
                horseMaterialObject.sharedMaterial = horseAndaluzMaterial[4];
                break;
        }
        Debug.Log("entrei");
        PlayerPrefs.SetString("HorseChoosed", Horse);
    }
    public void ChoosedHorse()
    {
        changedHorse = true;
    }

    public void resetMinigame()
    {
        SceneManager.LoadScene(PlayerPrefs.GetString("MinigameAt"));

    }

    public void EndMiniGame()
    {
        if (sceneLoaded == "MinigameOne")
        {
            carregandoEndUI.SetActive(true);
            StartCoroutine(CarregandoEnd("MinigameTwo"));
            Time.timeScale = 1;
        }
        if (sceneLoaded == "MinigameTwo")
        {
            carregandoEndUI.SetActive(true);
            StartCoroutine(CarregandoEnd("MinigameTen"));
            Time.timeScale = 1;
        }
        else if(sceneLoaded == "MinigameThree")
        {
            pointsControllerScript.AddPoints(500);
            axysCorrection.enabled = false;
            soundEffectController.PlayAudio("Criancas_Yay_Curto", true);
            carregandoEndUI.SetActive(true);
            StartCoroutine(CarregandoEnd("MinigameOne"));
            Time.timeScale = 1;
        }
        else if (sceneLoaded == "MinigameFour")
        {
            carregandoEndUI.SetActive(true);
            PlayerPrefs.SetInt("UnlockBelinha", 1);
            PlayerPrefs.SetInt("UnlockEquo", 1);
            StartCoroutine(CarregandoEnd("MinigameEight"));
            Time.timeScale = 1;
        }
        else if (sceneLoaded == "MinigameFive")
        {
            horseMovement.ResetPosition();
            velocityController.SetActive(true);
            finishedCutScene = false;
            Pre1Cutscene.SetActive(true);
            pointsControllerScript.AddPoints(500);
            StartCoroutine(CarregandoEndSameScene());
            sceneLoaded = "MinigameSeven";
            Time.timeScale = 1;
        }
        else if (sceneLoaded == "MinigameSix")
        {
            horseMovement.ResetPosition();
            velocityController.SetActive(true);
            finishedCutScene = false;
            //Pre2Cutscene.SetActive(true);
            carregandoEndUI.SetActive(true);
            pointsControllerScript.AddPoints(10 * (timeAttackInt));
            StartCoroutine(CarregandoEnd("MinigameNine"));
            Time.timeScale = 1;
        }
        else if(sceneLoaded == "MinigameSeven")
        {
            horseMovement.ResetPosition();
            timeAttackText.gameObject.SetActive(true);
            finishedCutScene = false;
            Pre3Cutscene.SetActive(true);
            pointsControllerScript.AddPoints(10 * (timeAttackInt));
            StartCoroutine(CarregandoEndSameScene());
            sceneLoaded = "MinigameSix";
            Time.timeScale = 1;
        }
        else if(sceneLoaded == "MinigameEight")
        {
            carregandoEndUI.SetActive(true);
            StartCoroutine(CarregandoEnd("MinigameNine"));
            pointsControllerScript.AddPoints(100);
            Time.timeScale = 1;
        }
        else if (sceneLoaded == "MinigameNine")
        {
            carregandoEndUI.SetActive(true);
            StartCoroutine(CarregandoEnd("MinigameNine2"));
            pointsControllerScript.AddPoints(100);
            Time.timeScale = 1;
        }
        else if (sceneLoaded == "MinigameNine2")
        {
            PlayerPrefs.SetInt("UnlockMax", 1);
            PlayerPrefs.SetInt("UnlockPre", 1);
            EndCutscene.SetActive(true);
            Time.timeScale = 1;
        }
        else if (sceneLoaded == "MinigameTen")
        {
            carregandoEndUI.SetActive(true);
            StartCoroutine(CarregandoEnd("MinigameFour"));
            Time.timeScale = 1;
        }
        else if(sceneLoaded == "MinigameTwelve")
        {
            pointsControllerScript.AddPoints(500);
            carregandoEndUI.SetActive(true);
            StartCoroutine(CarregandoEndSameScene());
            sceneLoaded = "BaiaTransitions";
            finishedBaiaMinigame = true;
        }
        else if(sceneLoaded == "MinigameThirteen")
        {
            pointsControllerScript.AddPoints(500);
            finishedBaiaMinigame = true;
            carregandoEndUI.SetActive(true);
            StartCoroutine(CarregandoEndSameScene());
            sceneLoaded = "BaiaTransitions";
            AlimentarCavaloButoons.SetActive(false);
        }
        else if(sceneLoaded == "MinigameFourteen")
        {
            pointsControllerScript.AddPoints(500);
            carregandoEndUI.SetActive(true);
            StartCoroutine(CarregandoEndSameScene());
            sceneLoaded = "BaiaTransitions";
            finishedBaiaMinigame = true;
        }
        else if(sceneLoaded == "BaiaTransitions")
        {
            carregandoEndUI.SetActive(true);
            PlayerPrefs.SetInt("UnlockBaia", 1);
            StartCoroutine(CarregandoEnd("MinigameThree"));
            Time.timeScale = 1;
        }
        pointsControllerScript.TotalPoints();
        if(pointsControllerScript.totalPoints >= 10000)
        {
            PlayerPrefs.SetInt("UnlockBill", 1);
        }
    }
    IEnumerator CarregandoEndSameScene()
    {
        yield return new WaitForSeconds(2f);
        carregandoEndUI.SetActive(false);
    }

    IEnumerator CarregandoEnd(string scene)
    {
        yield return new WaitForSeconds(1f);
        //Time.timeScale = 0;
        SceneManager.LoadScene(scene);
        Destroy(GameObject.FindGameObjectWithTag("Bgm"));
        if (scene == "MinigameEight")
        {
            if(sceneLoaded == "MinigameFour")
            {
                PlayerPrefs.SetString("MinigameAt", "MinigameFive");
            }
            else
            {
                PlayerPrefs.SetString("MinigameAt", sceneLoaded);
            }
        }
        else
        {
            PlayerPrefs.SetString("MinigameAt", scene);
        }
        
        
    }

    public void endedWinOrLoseDicas()
    {
        endedWinOrLose = true;
    }
    public void GoToMenu()
    {
        Destroy(GameObject.FindGameObjectWithTag("Bgm"));
        SceneManager.LoadScene("MenuInicial");
        Time.timeScale = 1;
    }
    public void FinishedCutscene()
    {
        StartCoroutine(TimeWaiting());
    }

    public void changeDicas(GameObject dicas)
    {
        StartCoroutine(timeToChangeDicas(dicas));
    }
    IEnumerator timeToChangeDicas(GameObject dicas)
    {
        yield return new WaitForSeconds(1f);
        dicas.SetActive(true);
    }

    public void setDicas()
    {
        setOneTime = true;
        dicasUnset = false;
        dicasAnim.Play("Meninas_Aparece");
        StartCoroutine(setingLoseOrWin());
    }
    public void unsetDicas()
    {
        Time.timeScale = 1;
        dicasUnset = true;
        StartCoroutine(unsetingDicasTime());
    }
    public void setDicasWin()
    {
        setOneTime = true;
        dicasUnset = false;
        dicasWin.SetActive(true);
        dicasAnim.Play("Meninas_Aparece");
        StartCoroutine(setingLoseOrWin());
    }

    public void setDicasLose()
    {
        setOneTime = true;
        dicasUnset = false;
        dicasLose.SetActive(true);
        dicasAnim.Play("Meninas_Aparece");
        StartCoroutine(setingLoseOrWin());
    }


    IEnumerator setingLoseOrWin()
    {
        yield return new WaitForSeconds(1f);
        horseMovement.stopHorse = true;
    }

    IEnumerator TimeWaiting()
    {
        yield return new WaitForSeconds(2f);
        finishedCutScene = true;
        setDicas();
    }
    IEnumerator unsetingDicasTime()
    {
        yield return new WaitForSeconds(1f);
        horseMovement.stopHorse = false;
    }
}
