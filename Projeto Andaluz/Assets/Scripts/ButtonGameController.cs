﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonGameController : MonoBehaviour
{
    public string buttonPressed;
    public string buttonToPress;
    public int imageRandomize;
    public Image ImageIndicator;

    public GameController gameControllerScript;

    public Sprite levantarMaos;
    public Sprite baterPalmas;
    public Sprite acenar;

    public Animator kidAnim;

    public float timeToWait;

    bool animationFinished = true;

    public PointsController pointsControllerScript;

    // Start is called before the first frame update
    void Start()
    {
        buttonToPress = "LevantarMaos";
    }

    // Update is called once per frame
    void Update()
    {
        if (!gameControllerScript.setOneTime)
        {
            return;
        }
        if(pointsControllerScript.pointsInLevel >= 1000)
        {
            gameControllerScript.setDicasWin();
            gameControllerScript.setOneTime = false;
        }
        if (!animationFinished)
        {
            return;
        }
        switch (buttonPressed)
        {
            case "LevantarMaos":
                if (buttonPressed == buttonToPress)
                {
                    do
                    {
                        imageRandomize = Random.Range(1, 4);
                    } while (imageRandomize == 1);
                    kidAnim.SetBool("LevantarMaos", true);
                    gameControllerScript.soundEffectController.PlayAudio("Criancas_Yay_Curto", true);
                    gameControllerScript.setParticles();
                    StartCoroutine(waintingAnimationTime(timeToWait));
                }
                else
                {
                    pointsControllerScript.LosePoints();
                    buttonPressed = "";
                    kidAnim.SetBool("LevantarMaos", true);
                    gameControllerScript.soundEffectController.PlayAudio("Bruhh_1", true);
                    gameControllerScript.setLoseMinigameParticles();
                    StartCoroutine(wrongAnim(2f));
                }
                break;
            case "Acenar":
                if (buttonPressed == buttonToPress)
                {
                    do
                    {
                        imageRandomize = Random.Range(1, 4);
                    } while (imageRandomize == 2);
                    kidAnim.SetBool("Acenar", true);
                    gameControllerScript.soundEffectController.PlayAudio("Criancas_Yay_Curto", true);
                    gameControllerScript.setParticles();
                    StartCoroutine(waintingAnimationTime(timeToWait));
                }
                else
                {
                    pointsControllerScript.LosePoints();
                    buttonPressed = "";
                    kidAnim.SetBool("Acenar", true);
                    gameControllerScript.soundEffectController.PlayAudio("Bruhh_1", true);
                    gameControllerScript.setLoseMinigameParticles();
                    StartCoroutine(wrongAnim(2f));
                }
                break;
            case "BaterPalmas":
                if (buttonPressed == buttonToPress)
                {
                    do
                    {
                        imageRandomize = Random.Range(1, 4);
                    } while (imageRandomize == 3);
                    kidAnim.SetBool("BaterPalmas", true);
                    gameControllerScript.soundEffectController.PlayAudio("Criancas_Yay_Curto", true);
                    gameControllerScript.setParticles();
                    StartCoroutine(waintingAnimationTime(timeToWait));
                }
                else
                {
                    pointsControllerScript.LosePoints();
                    buttonPressed = "";
                    kidAnim.SetBool("BaterPalmas", true);
                    gameControllerScript.soundEffectController.PlayAudio("Bruhh_1", true);
                    gameControllerScript.setLoseMinigameParticles();
                    StartCoroutine(wrongAnim(2f));
                }
                break;
            }
        }

    public IEnumerator waintingAnimationTime(float timeToWait)
    {
        float counter = 0f;
        animationFinished = false;
        while (counter < timeToWait)
        {
            counter += Time.deltaTime;
            yield return null;
        }
        ZeraAnim();
        SwitchImage(imageRandomize);
        animationFinished = true;
        pointsControllerScript.AddPoints(100);
    }

    public IEnumerator wrongAnim(float timeToWait)
    {
        animationFinished = false;
        yield return new WaitForSeconds(timeToWait);
        ZeraAnim();
        animationFinished = true;
    }
    public void SwitchImage(int image)
    {
        switch (image)
        {
            case 1:
                ImageIndicator.sprite = levantarMaos;
                buttonToPress = "LevantarMaos";
                break;
            case 2:
                ImageIndicator.sprite = acenar;
                buttonToPress = "Acenar";
                break;
            case 3:
                ImageIndicator.sprite = baterPalmas;
                buttonToPress = "BaterPalmas";
                break;
        }
        buttonPressed = "";
    }


    public void LevantarMaos()
    {
        buttonPressed = "LevantarMaos";
    }
    public void Acenar()
    {
        buttonPressed = "Acenar";
    }
    public void BaterPalmas()
    {
        buttonPressed = "BaterPalmas";
    }

    public void ZeraAnim()
    {
        kidAnim.SetBool("LevantarMaos", false);
        kidAnim.SetBool("Acenar", false);
        kidAnim.SetBool("BaterPalmas", false);
    }


}
