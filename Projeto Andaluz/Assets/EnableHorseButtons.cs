﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class EnableHorseButtons : MonoBehaviour
{

    public Button belinha;
    public Button max;
    public Button bill;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(PlayerPrefs.GetInt("UnlockBelinha") == 1 && !belinha.interactable)
        {
            belinha.interactable = true;
        }
        if(PlayerPrefs.GetInt("UnlockMax") == 1 && !max.interactable)
        {
            max.interactable = true;
        }
        if(PlayerPrefs.GetInt("UnlockBill") == 1 && !bill.interactable)
        {
            bill.interactable = true;
        }
    }
}
