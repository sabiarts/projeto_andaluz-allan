﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableObjectScript : MonoBehaviour
{
    public void DisableObject()
    {
        StartCoroutine(Disable());
    }

    IEnumerator Disable()
    {
        yield return new WaitForSeconds(1f);
        gameObject.SetActive(false);
    }
}
