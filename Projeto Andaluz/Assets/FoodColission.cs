﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodColission : MonoBehaviour
{
    public bool collidedWithCenoura;
    public bool collidedWithFeno;
    public bool collidedWithMaca;
    private void OnTriggerEnter(Collider other)
    {
        if(other.name == "cenoura")
        {
            collidedWithCenoura = true;
        }
        else if(other.name == "feno")
        {
            collidedWithFeno = true;
        }
        else if(other.name == "maca")
        {
            collidedWithMaca = true;
        }
    }
}
