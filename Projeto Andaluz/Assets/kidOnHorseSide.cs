﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class kidOnHorseSide : MonoBehaviour
{
    public Animator kidAnim;
    public Animator horseAnim;
    public MoveThrowPaths moveThrowPaths;

    // Update is called once per frame
    void Update()
    {
        if (moveThrowPaths.arrivedInLocation)
        {
            kidAnim.SetFloat("zAxys", 0);
            return;
        }
        if (horseAnim.GetBool("Walking"))
        {
            kidAnim.SetFloat("zAxys", 1);
        }
        else
        {
            kidAnim.SetFloat("zAxys", 0);
        }
    }
}
