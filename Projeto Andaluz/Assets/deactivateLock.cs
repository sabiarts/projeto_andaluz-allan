﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class deactivateLock : MonoBehaviour
{
    public Button button;
    public GameObject lockImage;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (button.interactable)
        {
            lockImage.SetActive(false);
        }
        else
        {
            lockImage.SetActive(true);
        }
    }
}
