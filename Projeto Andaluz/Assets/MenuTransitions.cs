﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuTransitions : MonoBehaviour
{
    public GameObject menuCanvas;
    public GameObject opcoesCanvas;
    public GameObject baiaCanvas;
    public GameObject equoCanvas;
    public GameObject preCanvas;
    public GameObject sabiartsCanvas;

    private Animator menuAnim;
    private Animator opcoesAnim;
    private Animator baiaAnim;
    private Animator equoAnim;
    private Animator preAnim;
    private Animator sabiartsAnim;

    // Start is called before the first frame update
    void Start()
    {
        menuAnim = menuCanvas.GetComponent<Animator>();
        opcoesAnim = opcoesCanvas.GetComponent<Animator>();
        baiaAnim = baiaCanvas.GetComponent<Animator>();
        equoAnim = equoCanvas.GetComponent<Animator>();
        preAnim = preCanvas.GetComponent<Animator>();
        sabiartsAnim = sabiartsCanvas.GetComponent<Animator>();
    }

    public void EnableCanvas(string canvasToEnable)
    {
        switch (canvasToEnable)
        {
            case "Menu":
                menuCanvas.SetActive(true);
                menuAnim.Play("Canvas_Aparece");
                break;
            case "Opcoes":
                opcoesCanvas.SetActive(true);
                opcoesAnim.Play("Canvas_Aparece");
                break;
            case "Baia":
                baiaCanvas.SetActive(true);
                baiaAnim.Play("Canvas_Aparece");
                break;
            case "Equo":
                equoCanvas.SetActive(true);
                equoAnim.Play("Canvas_Aparece");
                break;
            case "Pre":
                preCanvas.SetActive(true);
                preAnim.Play("Canvas_Aparece");
                break;
            case "Sabiarts":
                sabiartsCanvas.SetActive(true);
                sabiartsAnim.Play("Canvas_Aparece");
                break;
        }
    }

    public void DisableCanvas(string canvasToDisable)
    {
        switch (canvasToDisable)
        {
            case "Menu":
                menuAnim.Play("Canvas_Desaparece");
                StartCoroutine(DisableAnimTime(menuCanvas));
                break;
            case "Opcoes":
                opcoesAnim.Play("Canvas_Desaparece");
                StartCoroutine(DisableAnimTime(opcoesCanvas));
                break;
            case "Baia":
                baiaAnim.Play("Canvas_Desaparece");
                StartCoroutine(DisableAnimTime(baiaCanvas));
                break;
            case "Equo":
                equoAnim.Play("Canvas_Desaparece");
                StartCoroutine(DisableAnimTime(equoCanvas));
                break;
            case "Pre":
                preAnim.Play("Canvas_Desaparece");
                StartCoroutine(DisableAnimTime(preCanvas));
                break;
            case "Sabiarts":
                sabiartsAnim.Play("Canvas_Desaparece");
                StartCoroutine(DisableAnimTime(sabiartsCanvas));
                break;
        }
        
    }
    IEnumerator DisableAnimTime(GameObject canvas)
    {
        yield return new WaitForSeconds(0.3f);
        canvas.SetActive(false);
    }
}
