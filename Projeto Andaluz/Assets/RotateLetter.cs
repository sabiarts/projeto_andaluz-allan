﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateLetter : MonoBehaviour
{
    Transform tr;
    Vector3 rotationDirection = new Vector3(0,1,0);
    public float rotationSpeed;
    public void Start()
    {
        tr = GetComponent<Transform>();
    }
    public void Update()
    {
        tr.Rotate(rotationDirection, rotationSpeed);
    }
}
