﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnlockButtonsMenu : MonoBehaviour
{
    public Button verQuadrinhos;
    public Button baia;
    public Button equo;
    public Button pre;

    // Update is called once per frame
    void Update()
    {
        if(PlayerPrefs.GetInt("UnlockBaia") == 1)
        {
            baia.interactable = true;
        }
        else
        {
            baia.interactable = false;
        }
        if(PlayerPrefs.GetInt("UnlockEquo") == 1)
        {
            equo.interactable = true;
        }
        else
        {
            equo.interactable = false;
        }
        if (PlayerPrefs.GetInt("UnlockPre") == 1)
        {
            pre.interactable = true;
            verQuadrinhos.interactable = true;
        }
        else
        {
            pre.interactable = false;
            verQuadrinhos.interactable = false;
        }
    }
}
