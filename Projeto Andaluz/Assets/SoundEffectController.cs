﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundEffectController : MonoBehaviour
{
    // Start is called before the first frame update
    public AudioClip soundToPlay;
    AudioSource audioScr;
    void Start()
    {
        audioScr = this.GetComponent<AudioSource>();
    }

    public void PlayAudio(string audioToPlay, bool playOneTime)
    {
        if (playOneTime)
        {
            soundToPlay = Resources.Load<AudioClip>(audioToPlay);
            audioScr.clip = soundToPlay;
            audioScr.loop = false;
            audioScr.Play();
        }
        else
        {
            soundToPlay = Resources.Load<AudioClip>(audioToPlay);
            audioScr.clip = soundToPlay;
            audioScr.loop = true;
            audioScr.Play();
        }
    }

    public void StopAudio()
    {
        audioScr.Stop();
        audioScr.loop = false;
    }

    public void playSimpleAudio(string audioToPlay)
    {
        soundToPlay = Resources.Load<AudioClip>(audioToPlay);
        audioScr.clip = soundToPlay;
        audioScr.loop = false;
        audioScr.Play();
    }
}
