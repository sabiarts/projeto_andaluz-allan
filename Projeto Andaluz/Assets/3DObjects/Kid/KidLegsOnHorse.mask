%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: KidLegsOnHorse
  m_Mask: 01000000000000000000000001000000010000000000000000000000000000000000000001000000010000000000000000000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Armature.001
    m_Weight: 1
  - m_Path: Armature.001/controlQuadril
    m_Weight: 1
  - m_Path: Armature.001/controlQuadril/Quadril
    m_Weight: 1
  - m_Path: Armature.001/controlQuadril/Quadril/Cintura
    m_Weight: 1
  - m_Path: Armature.001/controlQuadril/Quadril/Cintura/Torax
    m_Weight: 1
  - m_Path: Armature.001/controlQuadril/Quadril/Cintura/Torax/Ombro.L
    m_Weight: 1
  - m_Path: "Armature.001/controlQuadril/Quadril/Cintura/Torax/Ombro.L/Bra\xE7o.L"
    m_Weight: 1
  - m_Path: "Armature.001/controlQuadril/Quadril/Cintura/Torax/Ombro.L/Bra\xE7o.L/Antebra\xE7o.L"
    m_Weight: 1
  - m_Path: "Armature.001/controlQuadril/Quadril/Cintura/Torax/Ombro.L/Bra\xE7o.L/Antebra\xE7o.L/DedaoBase.L"
    m_Weight: 1
  - m_Path: "Armature.001/controlQuadril/Quadril/Cintura/Torax/Ombro.L/Bra\xE7o.L/Antebra\xE7o.L/DedaoBase.L/DedaoPonta.L"
    m_Weight: 1
  - m_Path: "Armature.001/controlQuadril/Quadril/Cintura/Torax/Ombro.L/Bra\xE7o.L/Antebra\xE7o.L/DedaoBase.L/DedaoPonta.L/DedaoPonta.L_end"
    m_Weight: 1
  - m_Path: "Armature.001/controlQuadril/Quadril/Cintura/Torax/Ombro.L/Bra\xE7o.L/Antebra\xE7o.L/M\xE3o.L"
    m_Weight: 1
  - m_Path: "Armature.001/controlQuadril/Quadril/Cintura/Torax/Ombro.L/Bra\xE7o.L/Antebra\xE7o.L/M\xE3o.L/DedoBase.L"
    m_Weight: 1
  - m_Path: "Armature.001/controlQuadril/Quadril/Cintura/Torax/Ombro.L/Bra\xE7o.L/Antebra\xE7o.L/M\xE3o.L/DedoBase.L/DedoPonta.L"
    m_Weight: 1
  - m_Path: "Armature.001/controlQuadril/Quadril/Cintura/Torax/Ombro.L/Bra\xE7o.L/Antebra\xE7o.L/M\xE3o.L/DedoBase.L/DedoPonta.L/DedoPonta.L_end"
    m_Weight: 1
  - m_Path: Armature.001/controlQuadril/Quadril/Cintura/Torax/Ombro.R
    m_Weight: 1
  - m_Path: "Armature.001/controlQuadril/Quadril/Cintura/Torax/Ombro.R/Bra\xE7o.R"
    m_Weight: 1
  - m_Path: "Armature.001/controlQuadril/Quadril/Cintura/Torax/Ombro.R/Bra\xE7o.R/Antebra\xE7o.R"
    m_Weight: 1
  - m_Path: "Armature.001/controlQuadril/Quadril/Cintura/Torax/Ombro.R/Bra\xE7o.R/Antebra\xE7o.R/DedaoBase.R"
    m_Weight: 1
  - m_Path: "Armature.001/controlQuadril/Quadril/Cintura/Torax/Ombro.R/Bra\xE7o.R/Antebra\xE7o.R/DedaoBase.R/DedaoPonta.R"
    m_Weight: 1
  - m_Path: "Armature.001/controlQuadril/Quadril/Cintura/Torax/Ombro.R/Bra\xE7o.R/Antebra\xE7o.R/DedaoBase.R/DedaoPonta.R/DedaoPonta.R_end"
    m_Weight: 1
  - m_Path: "Armature.001/controlQuadril/Quadril/Cintura/Torax/Ombro.R/Bra\xE7o.R/Antebra\xE7o.R/M\xE3o.R"
    m_Weight: 1
  - m_Path: "Armature.001/controlQuadril/Quadril/Cintura/Torax/Ombro.R/Bra\xE7o.R/Antebra\xE7o.R/M\xE3o.R/DedoBase.R"
    m_Weight: 1
  - m_Path: "Armature.001/controlQuadril/Quadril/Cintura/Torax/Ombro.R/Bra\xE7o.R/Antebra\xE7o.R/M\xE3o.R/DedoBase.R/DedoPonta.R"
    m_Weight: 1
  - m_Path: "Armature.001/controlQuadril/Quadril/Cintura/Torax/Ombro.R/Bra\xE7o.R/Antebra\xE7o.R/M\xE3o.R/DedoBase.R/DedoPonta.R/DedoPonta.R_end"
    m_Weight: 1
  - m_Path: "Armature.001/controlQuadril/Quadril/Cintura/Torax/Pesco\xE7o"
    m_Weight: 1
  - m_Path: "Armature.001/controlQuadril/Quadril/Cintura/Torax/Pesco\xE7o/Cabe\xE7a"
    m_Weight: 1
  - m_Path: "Armature.001/controlQuadril/Quadril/Cintura/Torax/Pesco\xE7o/Cabe\xE7a/Cabe\xE7a_end"
    m_Weight: 1
  - m_Path: Armature.001/controlQuadril/Quadril/Coxa.L
    m_Weight: 1
  - m_Path: Armature.001/controlQuadril/Quadril/Coxa.L/Canela.L
    m_Weight: 1
  - m_Path: Armature.001/controlQuadril/Quadril/Coxa.L/Canela.L/Pe.L
    m_Weight: 1
  - m_Path: Armature.001/controlQuadril/Quadril/Coxa.L/Canela.L/Pe.L/Ponta.L
    m_Weight: 1
  - m_Path: Armature.001/controlQuadril/Quadril/Coxa.L/Canela.L/Pe.L/Ponta.L/Ponta.L_end
    m_Weight: 1
  - m_Path: Armature.001/controlQuadril/Quadril/Coxa.R
    m_Weight: 1
  - m_Path: Armature.001/controlQuadril/Quadril/Coxa.R/Canela.R
    m_Weight: 1
  - m_Path: Armature.001/controlQuadril/Quadril/Coxa.R/Canela.R/Pe.R
    m_Weight: 1
  - m_Path: Armature.001/controlQuadril/Quadril/Coxa.R/Canela.R/Pe.R/Ponta.R
    m_Weight: 1
  - m_Path: Armature.001/controlQuadril/Quadril/Coxa.R/Canela.R/Pe.R/Ponta.R/Ponta.R_end
    m_Weight: 1
  - m_Path: cabelo menina
    m_Weight: 1
  - m_Path: cabelo menino
    m_Weight: 1
  - m_Path: pessoinha
    m_Weight: 1
